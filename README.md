# Confidential issues

You can use this project to report Freeplane issues and mark them as confidential.
This functionality is not available at GitHub.
